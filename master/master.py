import sys
import os
import time
from subprocess import Popen, PIPE

out_path = '../master/out/' #relative to solver

cores = sys.argv[3]
inputs = sys.argv[2]
solver_path = sys.argv[1]
scores = {}
improve = []
try:
    flog = open('results.log', 'r')
    for l in flog.readlines():
        delim = l.split()
        scores[delim[0]] = delim[1]
        if len(delim) > 2:
           improve.append(delim[0] + '.in')
except IOError:
    pass
improving = True
if len(improve) == 0:
    improving = False
    improve = os.listdir(inputs)
start = time.time()
for fin in improve:
    if fin[:-3] not in scores.keys() or improving:
        print(fin[:-3])
        p = Popen([solver_path, inputs + fin, out_path + fin[:-3] + '.out', cores], stdout=PIPE)
        out = p.communicate()[0].splitlines()[0].decode("utf-8")
        p.wait()
        if float(out) < float(scores[fin[:-3]]):
            scores[fin[:-3]] = out
            print('improved!')
        else:
            print('no improvement')
        print(out)
        print ('t: ', time.time() - start)
        sys.stdout.flush()
        flog = open('results.log', 'w')
        for name in scores.keys():
            print(name + ' ' + scores[name], file=flog)
