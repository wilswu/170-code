#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <algorithm>
#include <unistd.h>
#include "ctpl_stl.h"

#define MAX 200
#define MUT_RATE 10
#define POP_SIZE 200
#define NEW_BLOOD 40
#define ITERS 1000
#define TOURN_SIZE 2

using namespace std;

int n;
int s;
string fname;
string names[MAX];
string g[MAX][MAX]; // original graph
double d[MAX][MAX]; // floyd distances
double g_d[MAX][MAX]; // edge distances
int nxt[MAX][MAX];
vector<int> pop[POP_SIZE];
int thr;
bool opted[POP_SIZE];
double mut_rate;

void print_seq(vector<int>& seq, ostream& stream) {
    if (seq.empty()) {
        stream << '\n';
        return;
    }
    for (int i = 0; i < seq.size() -1; i++) {
        stream << names[seq[i]] << ' ';
    }
    stream << names[seq[seq.size() - 1]] << '\n';
}

void floyd() {
    for (int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            d[i][j] = -1;
            g_d[i][j] = -1;
            nxt[i][j] = -1;
        }
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (i == j) {
                d[i][j] = 0;
                g_d[i][j] = 0;
            } else if (g[i][j] != "x") {
                d[i][j] = stod(g[i][j]);
                g_d[i][j] = d[i][j];
                nxt[i][j] = j;
            }
        }
    }    
    for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (d[i][k] >= 0 && d[k][j] >= 0) {
                    if (d[i][j] > d[i][k] + d[k][j] || d[i][j] < 0) {
                        d[i][j] = d[i][k] + d[k][j];
                        nxt[i][j] = nxt[i][k];
                    }
                }
            }
        }
    }
}

/** Returns vector of path b/w A and B NOT INCLUDING A BUT INCLUDING B. **/
vector<int> path (int a, int b) {
    if (nxt[a][b] < 0) {
        // cout << a << ' ' << b << '\n';
        // cout << "graph aint connected m8\n";
        return vector<int>();
    }
    vector<int> p;
    while (a != b) {
        a = nxt[a][b];
        p.push_back(a);
    }
    return p;
}

vector<int> full_path(vector<int>& gen) {
    vector<int> full_path;
    full_path.push_back(s);
    vector<int> part = path(s, gen[0]);
    full_path.insert(full_path.end(), part.begin(), part.end());
    for (int i = 1; i < gen.size(); i++) {
        part = path(gen[i - 1], gen[i]);
        full_path.insert(full_path.end(), part.begin(), part.end());        
    }
    part = path(gen[gen.size() - 1], s);
    full_path.insert(full_path.end(), part.begin(), part.end());        
    return full_path;
}

/** Cost of conquering v, preceded by u. **/
double cost(int u, int v) {
    return d[u][v] + stod(g[v][v]);
}

void make_feas_rand(vector<int> &gen) {
    int len = gen.size();
    bool v[n];
    for (int i = 0; i < n; i++) {
        v[i] = 0;
    }
    for (int i = 0; i < len; i++) {
        v[gen[i]] = 1;
        for (int j = 0; j < n; j++) {
            if (g_d[gen[i]][j] >= 0) {
                v[j] = 1;
            }
        }
    }
    bool covered = 1;
    for (int i = 0; i < n; i++) {
        if (!v[i]) {
            covered = 0;
        }
    }
    int cand;
    while (!covered) {
        cand = rand() % n;
        if (std::find(gen.begin(), gen.end(), cand) == gen.end()) {
            gen.insert(gen.begin() + rand() % gen.size() + 1, cand);
            for (int i = 0; i < n; i++) {
                if (g_d[cand][i] >= 0) {
                    v[i] = 1;
                }
            }
        }
        covered = 1;
        for (int i = 0; i < n; i++) {
            if (!v[i]) {
                covered = 0;
            }
        }
    }
}

void make_feas (vector<int> &gen) {
    // Each iteration, exhaustively test all possibilities of vertices and insertion locations.
    // Iterate until feasible.
    int len = gen.size();
    bool v[n];
    for (int i = 0; i < n; i++) {
        v[i] = 0;
    }
    for (int i = 0; i < len; i++) {
        v[gen[i]] = 1;
        for (int j = 0; j < n; j++) {
            if (g_d[gen[i]][j] >= 0) {
                v[j] = 1;
            }
        }
    }
    bool covered = 0;
    int best_pos;
    int best_v;
    double best_val = 0;
    double cover;
    double cur_cost;
    while (!covered) {
        covered = 1;
        for (int i = 0; i < n; i++) {
            if (!v[i]) {
                covered = 0;
            }
        }
        if (!covered) {
            best_val = 0;
            for (int i = 0; i < n; i++) {
                cover = 0;
                for (int j = 0; j < n; j++) {
                    if (!v[j] && g_d[i][j] >= 0) {
                        cover += stod(g[j][j]);
                    }
                }
                for (int j = 0; j <= len; j++) {
                    if (gen.size() == 0) {
                        cur_cost = cost(s, i) + d[i][s];
                    }
                    else if (j == 0) {
                        cur_cost = cost(s, i) + d[i][gen[0]] - d[s][gen[0]];
                    } else if (j == len) {
                        cur_cost = cost(gen[len - 1], i) + d[i][s] - d[gen[len - 1]][s];
                    } else {
                        cur_cost = cost(gen[j - 1], i) + d[i][gen[j]] - d[gen[j - 1]][gen[j]];
                    }
                    if (cur_cost < 0.00002) { //avoid divide by 0
                        cur_cost = 1;
                        cover *= 50000;
                    }
                    if (best_val < cover / cur_cost) {
                        best_val = cover / cur_cost;                        
                        best_v = i;
                        best_pos = j;
                        // cout << best_val << ' ' << best_v << ' ' << best_pos << '\n';
                    }
                }
            }            
            gen.insert(gen.begin() + best_pos, best_v);
            v[best_v] = 1;
            for (int i = 0; i < n; i++) {
                if (g_d[best_v][i] >= 0) {
                    v[i] = 1;
                }
            }
        }
    }
}

void make_feas_old (vector<int> &gen) {
//    cout << "STARTING FEAS\n";
    // for (int k = 0; k < gen.size(); k++) {
    //     cout << gen[k] << ' ';
    // }
    // cout << '\n';
    // print_seq(gen, cout);
    // cout << gen.size() << '\n';
    bool v[n];
    for (int i = 0; i < n; i++) {
        v[i] = 0;
    }
    for (int i = 0; i < gen.size(); i++) {
        v[gen[i]] = 1;
        for (int j = 0; j < n; j++) {
            if (g_d[gen[i]][j] >= 0) {
                v[j] = 1;
            }
        }        
    }
    bool covered = 0;
    while (!covered) {
        covered = 1;
        for (int i = 0; i < n; i++) {
//            cout << v[i];
            if (!v[i]) {
                covered = 0;
            }
        }
//        cout << '\n';
        if (!covered) {
            int best = 0;
            double best_cover = 0;
            double cover;
            ////////////////////////////////////////////////////////////////////////////////////
            // note that the following finds the vertex which covers the most cost, then
            // finds the cheapest location for it in the path. this is not necessarily the same
            // as finding the vector which maximizes covered / cost (but is a bit faster)
            ////////////////////////////////////////////////////////////////////////////////////
            // find vector which covers most:
            for (int i = 0; i < n; i++) {
                cover = 0;
                for (int j = 0; j < n; j++) {
                    if (!v[j] && g_d[i][j] >= 0) {
                        cover += stod(g[j][j]);
                    }
                }
                if (cover > best_cover) {
                    best = i;
                    best_cover = cover;
                }
            }
            //find best location to put best vector:
            int best_place = 0;
            double best_cost = cost(s, best);
            int cur_cost;
            for (int i = 0; i < gen.size(); i++) {
                cur_cost = cost(gen[i], best);
                if (best_cost < 0 || cur_cost < best_cost) {
                    best_cost = cur_cost;
                    best_place = i + 1;
                }
            }
            // cout << best_place << '\n';
            gen.insert(gen.begin() + best_place, best);
//            cout << "inserting " << best << " in" << best_place << '\n';
//            cout << gen.size() << '\n';
            for (int i = 0; i < n; i++) {
                if (g_d[best][i] >= 0) {
                    v[i] = 1;
                }
            }
        }
    }
}

void trim(vector<int> &gen) {
    int cover[n];
    for (int i = 0; i < n; i++) {
        cover[i] = 0;
    }
    for (int i = 0; i < gen.size(); i++) {
        for (int j = 0; j < n; j++) {
            if (g_d[gen[i]][j] >= 0) {
                cover[j]++;
            }
        }
    }
    bool done = 0;
    bool red;
    while (!done) {
        // for (int i = 0; i < n; i++) {
        //     cout << cover[i] << '\n';
        // }
        done = 1;
        for (int i = 0; i < gen.size(); i++) {
            red = 1;
            for (int j = 0; j < n; j++) {
                if (g_d[gen[i]][j] >= 0 && cover[j] <= 1) {
                    red = 0;
                }
            }
            if (red) {
                for (int j = 0; j < n; j++) {
                    if (g_d[gen[i]][j] >= 0) {
                        cover[j]--;
                    }
                }
                gen.erase(gen.begin() + i);
                i--;
                done = 0;
            }
        }
    }
}

double fit (vector<int> &gen) {
    double fit = cost(s, gen[0]);
    // cout << "size: " << gen.size() << '\n';
    // for (int i = 0; i < gen.size(); i++) {
    //     cout << gen[i] << ' ';
    // }
    // cout << '\n';
    for (int i = 1; i < gen.size(); i++) {
        // cout << i << '\n';
        // cout << gen[i - 1] << ' ' << gen[i] << '\n';
        fit += cost(gen[i - 1], gen[i]);
    }
    fit += d[gen[gen.size() - 1]][s];
    return fit;
}

void search(vector<int> &gen) {
    // cout << "Searching: ";
    // print_seq(gen, cout);
    vector<int> gen_copy(gen);
    for (int i = 0; i < gen_copy.size(); i++) {
        gen_copy.erase(gen_copy.begin() + i);
        make_feas(gen_copy);
        trim(gen_copy);
        if (fit(gen_copy) < fit(gen)) {            
            gen = vector<int>(gen_copy);
            i = 0;
            // cout << fit(gen_copy) << '\n';
        } else {
            gen_copy = vector<int>(gen);
        }
    }
}

void mut (vector<int> &gen) {
    // int rnd;
    // if (gen.size() < n / 2) {
    //     for (int i = 0; i < gen.size(); i++) {
    //         if (MUT_RATE > rand() % n) {
    //             do {
    //                 rnd = rand() % n;
    //             } while (find(gen.begin(), gen.end(), rnd) != gen.end());
    //             gen[i] =rnd;
    //         }
    //     }
    // }
    // for (int i = 0; i < gen.size(); i++) {
    //     int tmp;
    //     if (MUT_RATE > rand() % n) {
    //         tmp = gen[i];
    //         rnd = rand() % gen.size();
    //         gen[i] = gen[rnd];
    //         gen[rnd] = tmp;
    //     }
    // }
    for (int i = 0; i < gen.size(); i++) {
        if (rand() % (gen.size() * 100000) < mut_rate * 100000){
            gen[i] = rand() % n;
        }
    }
}

void opt3(vector<int> &gen){
    double cur_cost;
    double new_cost;
    double best_impr;
    int best_i, best_j, best_k, best_swap, swap;
    bool impr = 1;
    int t1, t2, t3, t4, t5, t6;
    // cout << "opt3!\n";
    // print_seq(gen, cout);
    while (impr) {
        impr = 0;
        best_impr = 0;
        for (int i = 0; i <= gen.size(); i++) {
            for (int j = 0; j < i; j++) {
                for (int k = 0; k < j; k++) {
                    t1 = k > 0 ? gen[k - 1] : s;
                    t2 = gen[k];
                    t3 = gen[j - 1];
                    t4 = gen[j];
                    t5 = gen[i - 1];
                    t6 = i < gen.size() ? gen[i] : s;
                    cur_cost = d[t1][t2] + d[t3][t4] + d[t5][t6];
                    new_cost = d[t1][t2] + d[t3][t4] + d[t5][t6];
                    if (d[t1][t2] + d[t3][t5] + d[t4][t6] < new_cost) {
                        swap = 1;
                        new_cost = d[t1][t2] + d[t3][t5] + d[t4][t6];
                    }
                    if (d[t1][t3] + d[t2][t4] + d[t5][t6] < new_cost) {
                        swap = 2;
                        new_cost = d[t1][t3] + d[t2][t4] + d[t5][t6];
                    }
                    if (d[t1][t3] + d[t2][t5] + d[t4][t6] < new_cost) {
                        swap = 3;
                        new_cost = d[t1][t3] + d[t2][t5] + d[t4][t6];
                    }
                    if (d[t1][t4] + d[t5][t2] + d[t3][t6] < new_cost) {
                        swap = 4;
                        new_cost = d[t1][t4] + d[t5][t2] + d[t3][t6];
                    }
                    if (d[t1][t4] + d[t5][t3] + d[t2][t6] < new_cost) {
                        swap = 5;
                        new_cost = d[t1][t4] + d[t5][t3] + d[t2][t6];
                    }
                    if (d[t1][t5] + d[t4][t2] + d[t3][t6] < new_cost) {
                        swap = 6;
                        new_cost = d[t1][t5] + d[t4][t2] + d[t3][t6];
                    }
                    if (d[t1][t5] + d[t4][t3] + d[t2][t6] < new_cost) {
                        swap = 7;
                        new_cost = d[t1][t5] + d[t4][t3] + d[t2][t6];
                    }
                    if (cur_cost - new_cost > best_impr + 0.00001) {
                        best_i = i;
                        best_j = j;
                        best_k = k;
                        best_impr = cur_cost - new_cost;
                        impr = 1;
                        best_swap = swap;
                    }
                }
            }
        }
        if (impr) {
            if (best_swap == 1) {
                reverse(gen.begin() + best_j, gen.begin() + best_i);
            } else if (best_swap == 2) {
                reverse(gen.begin() + best_k, gen.begin() + best_j);
            } else if (best_swap == 3) {
                reverse(gen.begin() + best_j, gen.begin() + best_i);
                reverse(gen.begin() + best_k, gen.begin() + best_j);
            } else if (best_swap == 4) {
                rotate(gen.begin() + best_k, gen.begin() + best_j, gen.begin() + best_i);
            } else if (best_swap == 5) {
                reverse(gen.begin() + best_k, gen.begin() + best_j);
                rotate(gen.begin() + best_k, gen.begin() + best_j, gen.begin() + best_i);
            } else if (best_swap == 6) {
                reverse(gen.begin() + best_j, gen.begin() + best_i);
                rotate(gen.begin() + best_k, gen.begin() + best_j, gen.begin() + best_i);
            } else if (best_swap == 7) {
                reverse(gen.begin() + best_k, gen.begin() + best_j);
                reverse(gen.begin() + best_j, gen.begin() + best_i);
                rotate(gen.begin() + best_k, gen.begin() + best_j, gen.begin() + best_i);
            }
        }
        // cout << best_swap << '\n';
        // cout << best_impr << '\n';
        // cout << fit(gen) << '\n';
    }
}

void opt2 (vector<int> &gen) {
    double cur_cost;
    double new_cost;
    double best_impr;
    int best_i;
    int best_j;
    bool impr = 1;
    while (impr) {
        impr = 0;
        best_impr = 0;
        for (int i = 0; i < gen.size(); i++) {
            for (int j = 0; j < i; j++) {
                cur_cost = (j > 0 ? d[gen[j - 1]][gen[j]] : d[s][gen[j]])
                    + (i < gen.size() - 1 ? d[gen[i]][gen[i + 1]] : d[gen[i]][s]);
                new_cost = (j > 0 ? d[gen[j - 1]][gen[i]] : d[s][gen[i]])
                    + (i < gen.size() - 1 ? d[gen[j]][gen[i + 1]] : d[gen[j]][s]);
                if (cur_cost - new_cost > best_impr) {
                    best_impr = cur_cost - new_cost;
                    best_i = i;
                    best_j = j;
                    impr = 1;
                }
            }
        }
        if (impr) {
            reverse(gen.begin() + best_j, gen.begin() + best_i + 1);
        }
    }
}

vector<int> cross (vector<int> &gen1, vector<int> &gen2) {
    int cross_point = rand() % min(gen1.size(), gen2.size());
    vector<int> child;
    child.insert(child.end(), gen1.begin(), gen1.begin() + cross_point + 1);
    child.insert(child.end(), gen2.begin() + cross_point + 1, gen2.end());
    return child;
}

vector<int> new_blood() {
    // int rnd;
    vector<int> gen;
    // for (int j = 0; j < rand() % n / 2 + 1; j++) {
    //     do {
    //         rnd = rand() % n;
    //     } while (find(gen.begin(), gen.end(), rnd) != gen.end());
    //     gen.push_back(rnd);
    // }
    // gen.clear();
    for (int i = 0; i < n; i++) {
        if (rand() % 2 == 0) {
            gen.push_back(i);
        }
    }
    // cout << "original" << '\n';
    // print_seq(gen, cout);
    // make_feas(gen);
    // cout << "feas" << '\n';
    // print_seq(gen, cout);    
    // trim(gen);
    // cout << "trim" << '\n';
    // print_seq(gen, cout);
    // opt2(gen);
    // cout << "opt" << '\n';
    // print_seq(gen, cout);
    return gen;
}

void opter(int id, int p, vector<int> &gen, bool m) {
    // cout << "id: " << id << "| ";
    // print_seq(gen, cout);
    if (m) {
        mut(gen);
    }
    make_feas(gen);
    trim(gen);
    search(gen);
    opt3(gen);    
    // print_seq(gen, cout);
    opted[p] = 1;
}

vector<int> iter_pop() { //returns best
    vector<int> best;
    double best_fit = -1;
    double cur_fit;
    ctpl::thread_pool pool(thr);
    memset(opted, 0, POP_SIZE);
    for (int i = 0; i < POP_SIZE; i++) {
        cur_fit = fit(pop[i]);
        if (cur_fit < best_fit || best_fit < 0) {
            best = pop[i];
            best_fit = cur_fit;
        }
    }
    best = vector<int>(best);    
    vector<int> new_pop[POP_SIZE];
    for (int i = NEW_BLOOD / 2; i < POP_SIZE / 2; i++) {
        // cout << i << '\n';
        vector<int> t1[TOURN_SIZE], t2[TOURN_SIZE];
        for (int j = 0; j < TOURN_SIZE; j++) {
            t1[j] = pop[rand() % POP_SIZE];
            t2[j] = pop[rand() % POP_SIZE];
        }
        vector<int> p1 = t1[0];
        vector<int> p2 = t2[0];
        for (int j = 1; j < TOURN_SIZE; j++) {
            if (fit(t1[j]) < fit(p1)) { // want "lower" fitness
                p1 = t1[j];
            }
            if (fit(t2[j]) < fit(p2)) {
                p2 = t2[j];
            }
        }
        // cout << "hey\n";
        new_pop[2 * i] = cross(p1, p2);
        new_pop[2 * i + 1] = cross(p1, p2);
        // print_seq(new_pop[2 * i], cout);
        // print_seq(new_pop[2 * i + 1], cout);
    }
    for (int i = 0; i < NEW_BLOOD; i++) {
        new_pop[i] = new_blood();        
        // opter(0, i, new_pop[i], false);
        pool.push(opter, i, ref(new_pop[i]), false);
    }
    for (int i = NEW_BLOOD; i < POP_SIZE; i++) {
        // cout << "original: " << '\n';
        // print_seq(new_pop[i], cout);
        // mut(new_pop[i]);
        // cout << "mutate: " << '\n';
        // print_seq(new_pop[i], cout);
        // make_feas(new_pop[i]);
        // cout << "feasible: " << '\n';
        // print_seq(new_pop[i], cout);
        // trim(new_pop[i]);
        // cout << "trimmed: " << '\n';
        // print_seq(new_pop[i], cout);
        // opt2(new_pop[i]);
        // opter(0, i, new_pop[i], true);
        pool.push(opter, i, ref(new_pop[i]), true);
    }
    pool.stop(true);
    for (int i = 0; i < POP_SIZE; i++) {
        if (!opted[i]) {
            cout << i << " not opted!\n";
            opter(0, i, new_pop[i], false);
        }
    }
    for (int i = 0; i < POP_SIZE; i++) {
        pop[i] = new_pop[i];
    }
    pop[0] = best; //forward best to next generation
    return best;
}

void init_pop() {
    ctpl::thread_pool pool(thr);
    memset(opted, 0, POP_SIZE);
    for (int i = 0; i < POP_SIZE; i++) {
        pop[i] = new_blood();
        pool.push(opter, i, ref(pop[i]), false);
        // opter(0, i, pop[i], false);
    }
    pool.stop(true);
    for (int i = 0; i < POP_SIZE; i++) {
        if (!opted[i]) {
            cout << i << " not opted!\n";
            opter(0, i, pop[i], false);
        }
    }
}

int main(int argc, char *argv[]) {
    srand(time(NULL));
    if (argc < 3 || argc > 5) {
        cerr << "Wrong number of arguments (need input and optionally, # threads).";
        return EXIT_FAILURE;
    }
    thr = argc == 4 ? stoi(argv[3]) : 1;
    string fin = argv[1];
    string fout = argv[2];
    // ifstream in("in/" + fname + ".in");
    // ofstream out("out/" + fname + ".out");
    ifstream in(fin);
    ofstream out(fout);
    ofstream log("log/" + fin.substr(fin.size() - 3) + ".log");
    in >> n;
    for (int i = 0; i < n; i++) {
        in >> names[i];
    }
    string s_str;
    in >> s_str;
    for (int i = 0; i < n; i++) {
        if (s_str == names[i]) {
            s = i;
        }
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            in >> g[i][j];
        }
    }
    cout << "Running Floyd-Warshall...";
    floyd();
    cout << " done.\n" << "Generating initial population...";
    init_pop();
    cout<<" done.\n" << "Starting iterations...\n";
    vector<int> best;
    time_t start = time(0);
    mut_rate = MUT_RATE;
    for (int i = 0; i < ITERS; i++) {
        best = iter_pop();
        // double best_fit = -1;
        // double cur_fit;
        // for (int i = 0; i < POP_SIZE; i++) {
        //     cur_fit = fit(pop[i]);
        //     if (cur_fit < best_fit || best_fit < 0) {
        //         best = pop[i];
        //         best_fit = cur_fit;
        //     }
        // }
        /////////////////////////GOOD OUTPUTS:////////////////////////////////////        
        cout << "Iteration: " << i << ", t=" << time(0) - start << '\n';
        cout << "Best Fitness: " << fit(best) << '\n';
        cout << "Best Genome: ";
        print_seq(best, cout);
        cout << '\n';
        ////////////////////////////////////////////////////////////////////////////
        log << fit(best) << '\n';
        if (time(0) - start > 3600) {
            break;
        }
        // for (int j = 0; j < 20; j++) {
        //     print_seq(pop[j], cout);
        // }
        mut_rate *= 0.999;
    }
    // cout<<"Done.\n";
    // cout<<"Printing output...";
    vector<int> path = full_path(best);
    print_seq(path, out);
    print_seq(best, out);
    cout << fit(best) << '\n';
    // cout<<" done.\n";
}
