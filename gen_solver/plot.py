import sys
import matplotlib.pyplot as plt

f = open(sys.argv[1], 'r')
data = f.read().splitlines()
plt.plot([float(d) for d in data])
plt.ylabel('fitness')
plt.xlabel('iteration')
plt.show()
