import random as rnd

def dist(a, b):
    """Returns Euclidian distance between points A and B."""
    d = 0
    for i in range(len(a)):
        d += (a[i] - b[i])**2
    return int(d**0.5)

def floyd(g):
    """Returns all-pairs shortest path lengths of graph G."""
    n = len(g)
    d = [[-1 for i in range(n)] for j in range(n)]
    nxt = [[-1 for i in range(n)] for j in range(n)]
    for i in range(n):
        for j in range(n):
            if i == j:
                d[i][j] = 0
            elif g[i][j] != 'x':
                d[i][j] = float(g[i][j])
                nxt[i][j] = j
    for k in range(n):
        for i in range(n):
            for j in range(n):
                if d[i][k] >= 0 and d[k][j] >= 0:
                    if d[i][j] == -1 or d[i][j] > d[i][k] + d[k][j]:
                        d[i][j] = d[i][k] + d[k][j]
                        nxt[i][j] = nxt[i][k]
    return d, nxt

def sh_path(a, b, d, nxt):
    """Returns shortest path between A and B given dists D,
    including A but not including B."""
    if nxt[a][b] < 0:
        return vector<int>()
    p = []
    while a != b:
        a = nxt[a][b]
        p.append(a)
    return p

def full_path(sol, s, d, nxt):
    """Returns full path from solution string."""
    full_path = []
    full_path.append(s)
    full_path.extend(sh_path(s, sol[0], d, nxt))
    for i in range(1, len(sol)):
        full_path.extend(sh_path(sol[i - 1], sol[i], d, nxt))
    full_path.extend(sh_path(sol[-1], s, d, nxt))
    return full_path
    
def verify(g):
    """Returns True iff graph G is indeed metric."""
    d, nxt = floyd(g)
    n = len(g)
    metric = True
    for i in range(len(g)):
        for j in range(len(g)):
            if i != j and g[i][j] != 'x':
                if d[i][j] < g[i][j]:
                    metric = False
    return metric

def metric(n, size):
    """Return a random complete metric graph with N vertices in
    adjacency array format. The graph can be represented as a n-dimensional
    polytope bounded by the n-hypercube with side length 2 * SIZE"""
    rnd.seed()
    points = []
    for i in range(n):
        points.append([rnd.randint(-size, size) for i in range(n)])
    d = [[dist(points[i], points[j])for i in range(n)] for j in range(n)]
    return d

def connected(g):
    """Returns if G is connected."""
    n = len(g)
    visited = [False for i in range(n)]
    def dfs(g, v):
        if visited[v]:
            return
        visited[v] = True
        for i in range(n):
            if g[v][i] != 'x':
                dfs(g, i)
    dfs(g, 0)
    return all(visited)

def rand_input(n, m, size, c_cost):
    """Output a random input file with N vertices and M edges, with distances
    bounded by a n-hypercube of side length 2*SIZE and conquering costs
    bounded by C_COST."""
    rnd.seed()
    g = metric(n,size)
    #print(g)
    print('g is metric' if verify(g) else 'G IS NOT METRIC')
    g = [[str(g[i][j]) for i in range(n)] for j in range(n)]
    edges = []
    for i in range(n):
        for j in range(n):
            if g[i][j] != 'x' and i != j:
                edges.append((i, j))
    while connected(g) and len(edges) > m * 2:
        print("edges:", len(edges) / 2)
        i, j = rnd.choice(edges)
        e = g[i][j]
        g[i][j] = 'x'
        g[j][i] = 'x'
        edges.remove((i, j))
        edges.remove((j, i))
        if not connected(g):
            g[i][j] = e
            g[j][i] = e
            edges.append((i, j))
            edges.append((j, i))
            break
    print('actual # edges in g is ', len(edges) / 2)
    for i in range(n):
        g[i][i] = str(rnd.randint(0, c_cost))
    fname = 'rand-' + str(n) + '-' + str(m) + '-' + str(size) + '-' \
            + str(c_cost) + '.in'
    f = open(fname, 'w')
    f.write(str(n) + '\n')
    f.write(' '.join(str(i) for i in range(n)) + '\n')
    f.write('0\n')
    for i in range(n):
        f.write(' '.join(g[i]) + '\n')

def hard_input(n, m, size, m_cost, c_cost, d):
    """Output a hard random input file with N vertices and M edges, distances
       bounded by SIZE, conquering costs M_COST or higher, bounded by 
       M_COST + C_COST. Optimal conquering path must contain D vertices."""
    rnd.seed()
    comp = metric(n,size)
    x = [i for i in range(d)]
    y = [i + d for i in range(d)]
    V = [[] for i in range(d)]
    for i in range(d, n):
        V[i % d].append(i)
    g = [['x' for i in range(n)] for j in range(n)]
    for i in range(d):
        for v in V[i]:
            g[x[i]][v] = str(comp[x[i]][v])
            g[v][x[i]] = str(comp[v][x[i]])
    edges = []
    for i in range(d, n):
        for j in range(d, n):
            if i != j:
                if i in y:
                    if j in V[y.index(i)]:
                        edges.append((i, j))
                elif j in y:
                    if i in V[y.index(j)]:
                        edges.append((i, j))
                else:
                    edges.append((i, j))
    print(len(edges))
    for i in range(m):
        # v = rnd.randrange(d, n)
        # if v in y:
        #     u = rnd.choice(V[y.index(v)])
        # else:
        #     u = rnd.randrange(n)
        #     while u in x:
        #         u = rnd.randrange(n)
        # g[v][u] = str(comp[v][u])
        # g[u][v] = str(comp[u][v])
        e = rnd.choice(edges)
        g[e[0]][e[1]] = str(comp[e[0]][e[1]])
        g[e[1]][e[0]] = str(comp[e[1]][e[0]])
        edges.remove(e)
        edges.remove((e[1], e[0]))
    for v in range(n):
        if v in x:
            g[v][v] = str(m_cost - rnd.randrange(100)) # oh no magic number
        else:
            g[v][v] = str(m_cost + rnd.randrange(c_cost))
    for i in range(1, d):
        g[i - 1][i] = str(comp[i - 1][i])
        g[i][i - 1] = str(comp[i][i - 1])
    # print('g is metric' if verify(g) else 'G IS NOT METRIC')
    fname = 'hard-' + str(n) + '-' + str(m) + '-' + str(size) + '-' \
            + str(c_cost) + '-' + str(d) + '.in'
    f = open(fname, 'w')
    f.write(str(n) + '\n')
    f.write(' '.join(str(i) for i in range(n)) + '\n')
    f.write('0\n')
    for i in range(n):
        f.write(' '.join(g[i]) + '\n')
    

def check(fin_name, fout_name):
    """Return total cost if input/output files FNAME are feasible, else -1"""
    fin = open(fin_name + '.in', 'r')
    fout = open(fout_name + '.out', 'r')
    n = int(fin.readline()[:-1])
    names = fin.readline()[:-1].split(' ')
    s = names.index(fin.readline()[:-1])
    g = [fin.readline()[:-1].split(' ') for i in range(n)]
    path = [names.index(name) for name in fout.readline()[:-1].split(' ') if name]
    conq = [names.index(name) for name in fout.readline()[:-1].split(' ') if name]
    good = True
    for i in range(len(path) - 1):
        if g[path[i]][path[i + 1]] == 'x':
            good = False
            print("Fuck", path[i], path[i + 1], g[path[i]][path[i + 1]])
    v = [False for i in range(n)]
    print("g is connected" if connected(g) else "piece of shit")
    for vrt in conq:
        for i in range(n):
            if g[vrt][i] != 'x':
                v[i] = True
    if not all(v):
        good = False
        print("Fuck 2")
    if not good:
        return -1
    cost = 0
    for i in range(len(path) - 1):
        cost += float(g[path[i]][path[i + 1]])
    for vrt in conq:
        cost += float(g[vrt][vrt])
    return cost

def cost(u, v, g, d):
    """Returns cost of conquering V preceded by U, given graph G and dists D."""
    return d[u][v] + float(g[v][v])

def fit(sol, g, s):
    """Returns cost of an ordered list of conquered kingdoms S on graph G,
    using shortest paths in between."""
    d, nxt = floyd(g)
    fit = cost(s, sol[0], g, d)
    for i in range(1, len(sol)):
        fit += cost(sol[i - 1], sol[i], g, d)
    fit += d[sol[-1]][s]
    return fit


    
