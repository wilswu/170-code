import sys
sys.path.insert(0, '../utils')
import math
import utils
from gurobipy import *

fname = sys.argv[1]
fin = open(fname + '.in', 'r')
fout = open(fname + '.out', 'w')

print("Reading in.")
n = int(fin.readline()[:-1])
names = fin.readline()[:-1].split(' ')
s = names.index(fin.readline()[:-1])
g = [fin.readline()[:-1].split(' ') for i in range(n)]

d, nxt = utils.floyd(g)

m = Model()
vars = {}

print("Creating variables.")
for t in range(n):
    for i in range(n):
        vars[i, t] = m.addVar(vtype = GRB.BINARY, name = "v " + str(i) + "t " + str(t))
        vars['end', t] = m.addVar(vtype = GRB.BINARY, name = "end: t " + str(t))
m.update()

print("Full cover constraints.")
# Full cover.
for i in range(n):
    # m.addConstr(quicksum([vars[i, t] for t in range(n)])\
    #             + quicksum([quicksum([vars[j, t] for j in range(n)]) for t in range(n)]))
   m.addConstr(quicksum(vars[i, t] + quicksum(vars[j, t] for j in range(n) if g[i][j] != 'x') for t in range(n)) >= 1)

print("Nonsimultaneity constraints.")
# Each time step has <= 1 kingdoms. After time step with 0 kingdoms, no more.
for t in range(n):
    m.addConstr(quicksum(vars[i, t] for i in range(n)) <= 1)
    if (t > 0):
        m.addConstr(quicksum(vars[i, t] for i in range(n))\
                    <= quicksum(vars[i, t - 1] for i in range(n)))
m.update()

print("End variable constraints.")
# Special end variable only 1 at last kingdom and all t after.
for t in range(n - 1):
    m.addConstr(vars['end', t] == 1 - quicksum(vars[i, t + 1] for i in range(n)))
#    m.addGenConstrIndicator(vars['end', t], True,\
#                            quicksum(vars[i, t + 1] for i in range(n)) == 0)
m.addConstr(vars['end', n - 1] == 1)
m.update()

print("Uniqueness constraints.")
# Every kingdom appears at most once.
for i in range(n):
    m.addConstr(quicksum(vars[i, t] for t in range(n)) <= 1)
m.update()

print("Objective function.")
m.setObjective(quicksum(vars[i, 0] * utils.cost(s, i, g, d) for i in range(n)) \
               + quicksum(quicksum(quicksum(\
                utils.cost(j, i, g, d) * vars[j, t - 1] * vars[i, t] \
                for j in range(n)) for i in range(n)) for t in range(1, n))\
               + quicksum(quicksum(vars['end', t] * vars[i, t] * d[i][s]\
                                   for t in range(n)) for i in range(n)),
               GRB.MINIMIZE)

m.update()
print("Optimizing...")
m.optimize()

sol = []

for t in range(n):
    for i in range(n):
        if vars[i, t].x > 0:
            sol.append(i)

print(utils.full_path(sol, s, d, nxt), file=fout)
print(sol, file=fout)

print(utils.full_path(sol, s, d, nxt))
print(sol)
print(utils.fit(sol, g, s))

# for t in range(n):
#     for i in range(n):
#         print(i, t, vars[i, t].x)

# for t in range(n):
#     print('end:', t, vars['end', t].x)
    
